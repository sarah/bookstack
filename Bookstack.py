import base64
import json
import requests


def get_auth_header(token_id, token_secret):
    return {"Authorization": "Token {}:{}".format(token_id, token_secret)}


def get_page_html(wiki_url, token_id, token_secret, page_id):
    headers = {}
    headers.update(get_auth_header(token_id, token_secret))

    r = requests.get(wiki_url + "/api/pages/{}/export/html".format(page_id), headers=headers)

    return r.text


def get_attachments(wiki_url, token_id, token_secret):
    headers = {}
    headers.update(get_auth_header(token_id, token_secret))

    r = requests.get(wiki_url + "/api/attachments", headers=headers)

    attachment_data = json.loads(r.text)['data']

    return attachment_data


def get_attachment_id(wiki_url, token_id, token_secret, attachment_name, page_id):
    attachment_data = get_attachments(wiki_url, token_id, token_secret)

    for attachment in attachment_data:
        if attachment['name'] == attachment_name and attachment['uploaded_to'] == page_id:
            return attachment['id']

    return None


def post_attachment(wiki_url, token_id, token_secret, filename, filedata, page_id):
    headers = {}
    headers.update(get_auth_header(token_id, token_secret))

    data = {}
    data["name"] = filename
    data["uploaded_to"] = page_id

    files = {}
    files["file"] = filedata

    requests.post(wiki_url + "/api/attachments", data=data, files=files, headers=headers)


def get_attachment(wiki_url, token_id, token_secret, attachment_id):
    headers = {}
    headers.update(get_auth_header(token_id, token_secret))

    r = requests.get(wiki_url + "/api/attachments/{}".format(attachment_id), headers=headers)
    attachment_data_encoded = json.loads(r.text)['content']
    attachment_data_decoded = base64.b64decode(attachment_data_encoded)

    return attachment_data_decoded


def update_attachment(wiki_url, token_id, token_secret, attachment_id, file_name, file_data, page_id):
    headers = {}
    headers.update(get_auth_header(token_id, token_secret))

    data = {}
    # Workaround for updating files: https://github.com/BookStackApp/BookStack/issues/3666
    data["_method"] = "PUT"
    data["name"] = file_name
    data["uploaded_to"] = page_id

    files = {}
    files["file"] = file_data

    requests.post(wiki_url + "/api/attachments/{}".format(attachment_id), headers=headers, data=data, files=files)


def delete_attachment(wiki_url, token_id, token_secret, attachment_id):
    headers = {}
    headers.update(get_auth_header(token_id, token_secret))

    requests.delete(wiki_url + "/api/attachments/{}".format(attachment_id), headers=headers)


def update_page(wiki_url, token_id, token_secret, page_id, markdown=None, html=None):
    headers = {}
    headers.update(get_auth_header(token_id, token_secret))

    data = {}
    data['markdown'] = markdown
    data['html'] = html

    requests.put(wiki_url + "/api/pages/{}".format(page_id), data=data, headers=headers)
